# s3transfer

This app creates a REST API that you can call to upload or download files from s3

## Initial setup:

Before first use you should run the following commands:

```bash
virtualenv venv
source venv/bin/activate
pip install -r requirements.txt
```

## Configure the REST API

In the _rest_api.py_ file you can edit the following two variables (found at the top of the file):

* api_port
* bucket_name

## To startup the REST API

```bash
./rest_api.py
```

## To use

```bash
curl -F "file=@<filename>" -X POST http://localhost:5000/upload
curl http://localhost:5000/download/<filename>
```

Note: You can also use your favourite web browser to download a file by navigating to `http://localhost:5000/download/<filename>`

## To Test

```bash
pytest -v
```

_Note: You don't have to have the Flask REST API server app running to perform the tests._
