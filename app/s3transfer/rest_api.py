#!/usr/bin/env python3
import os
import urllib.request
import boto3
# import logging
from botocore.exceptions import ClientError
from app import app
from flask import Flask, request, redirect, jsonify, Response, stream_with_context

s3 = boto3.client('s3')

bucket_name = 'v-nova-devops-test-s3transfer'
api_port = '5000'


@app.route('/upload', methods=['POST'])
def upload_file():
    """File upload handler"""
    global s3

    # check if the post request has the file part
    if 'file' in request.files:
        file = request.files['file']
        if file.filename == '':
            resp = jsonify({'message': 'No file selected for uploading'})
            resp.status_code = 400
            return resp

        result = s3.put_object(Bucket=bucket_name,
                               Key=file.filename,
                               Body=file.stream)
        resp = jsonify({'message': 'File successfully uploaded to S3'})
        resp.status_code = 201
        return resp
    else:
        resp = jsonify({'message': 'No file in the request'})
        resp.status_code = 400
        return resp


@app.route('/download/<s3_key>', methods=['GET'])
def download_file(s3_key):
    """File download handler"""
    global s3

    try:
        s3obj = s3.get_object(Bucket=bucket_name,
                              Key=s3_key)
        resp = Response(stream_with_context(
            s3obj['Body']), content_type=s3obj['ContentType'])
        print(resp.headers)
        resp.headers.set('Content-Disposition', 'attachment', filename=s3_key)
        resp.status_code = 200
    except ClientError as ex:
        if ex.response['Error']['Code'] == 'NoSuchKey':
            resp = jsonify({'message': 'No object with that key was found'})
            resp.status_code = 404
        else:
            resp = jsonify(
                {'message': 'Unknown aws client error: {}.'.format(ex)})
            resp.status_code = 500
    except Exception as ex:
        resp = jsonify({'message': 'Unknown error: {}.'.format(ex)})
        resp.status_code = 500

    return resp


if __name__ == "__main__":
    print('Using bucket {} to upload to.'.format(bucket_name))
    print('Access this API on: http://localhost:{}'.format(api_port))

    # Uncomment this section (plus the include a the top of the file)
    # to enable flask debug logging:
    # logging.basicConfig(
    #     level=logging.DEBUG,
    #     format=f'%(asctime)s %(levelname)s %(message)s'
    # )
    # logger = logging.getLogger()

    app.run(port=api_port)
