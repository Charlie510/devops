import os
import io
import tempfile
import pytest

from rest_api import app


@pytest.fixture
def client():
    app.config['TESTING'] = True
    with app.test_client() as client:
        yield client


def test_upload_file(client):
    data = {}
    data['file'] = (io.BytesIO(b"Hello World!"), 'test.txt')
    rv = client.post('/upload', data=data, content_type='multipart/form-data')
    assert rv.status_code == 201 and b'File successfully uploaded to S3' in rv.data, "Response status: {}. Response text: {}".format(
        drv.status_code, drv.data)


def test_upload_file_nonexistant(client):
    rv = client.post('/upload')
    assert rv.status_code == 400 and b'No file in the request' in rv.data, "Response status: {}. Response text: {}".format(
        drv.status_code, drv.data)


def test_download_s3key_nonexistant(client):
    rv = client.get('/download/invalid_key')
    assert rv.status_code == 404 and b'No object with that key was found' in rv.data, "Response status: {}. Response text: {}".format(
        drv.status_code, drv.data)


def test_upload_and_download_of_file(client):
    data = {}
    data['file'] = (io.BytesIO(b"Hello World!"), 'test.txt')
    urv = client.post('/upload', data=data, content_type='multipart/form-data')
    assert b'File successfully uploaded to S3' in urv.data
    drv = client.get('/download/test.txt')
    assert drv.status_code == 200 and drv.data == b'Hello World!', "Response status: {}. Response text: {}".format(
        drv.status_code, drv.data)


def test_invalid_upload_method(client):
    rv = client.post('/upload/something')
    assert rv.status_code == 404 and b'<title>404 Not Found</title>' in rv.data


def test_invalid_method(client):
    rv = client.post('/unknown')
    assert rv.status_code == 404 and b'<title>404 Not Found</title>' in rv.data
