# resource "aws_instance" "bastion" {
#   ami             = data.aws_ami.amazon_linux.id
#   instance_type   = var.instance_type
#   security_groups = [aws_security_group.bastion.id]
#   key_name        = aws_key_pair.access.id
#   subnet_id       = module.vpc.public_subnets[0]
#   tags            = var.default_tags
# }

# resource "aws_instance" "web" {
#   ami             = var.built_for_test_ami
#   instance_type   = var.instance_type
#   security_groups = [aws_security_group.web_server.id]
#   key_name        = aws_key_pair.access.id
#   subnet_id       = module.vpc.private_subnets[0]
#   tags            = var.default_tags
# }

resource "aws_key_pair" "access" {
  key_name   = "access-key"
  public_key = file(var.sshpubkey_file)
}
############################################################################

resource "aws_launch_template" "bastion" {
  name_prefix   = "bastion"
  image_id      = data.aws_ami.amazon_linux.id
  instance_type = var.instance_type
  key_name      = aws_key_pair.access.id
  network_interfaces {
    associate_public_ip_address = true
    security_groups             = [aws_security_group.bastion.id]
    subnet_id                   = module.vpc.public_subnets[0]
  }
  tags = var.default_tags
}

resource "aws_autoscaling_group" "bastion" {
  vpc_zone_identifier = [module.vpc.public_subnets[0]]
  desired_capacity    = 1
  max_size            = 1
  min_size            = 1

  launch_template {
    id      = aws_launch_template.bastion.id
    version = "$Latest"
  }
}

resource "aws_launch_template" "web" {
  name_prefix   = "web"
  image_id      = var.built_for_test_ami
  instance_type = var.instance_type
  key_name      = aws_key_pair.access.id
  network_interfaces {
    associate_public_ip_address = false
    security_groups             = [aws_security_group.web_server.id]
    subnet_id                   = module.vpc.private_subnets[0]
  }
  tags = var.default_tags
}

resource "aws_autoscaling_group" "web" {
  vpc_zone_identifier = [module.vpc.private_subnets[0]]
  target_group_arns   = [aws_lb_target_group.front_end.id]
  desired_capacity    = 1
  max_size            = 1
  min_size            = 1
  health_check_type   = "ELB"
  force_delete        = true

  launch_template {
    id      = aws_launch_template.web.id
    version = "$Latest"
  }
}
