module "vpc" {
  name   = "devops-test-vpc"
  source = "terraform-aws-modules/vpc/aws"

  cidr = var.vpc_cidr

  azs = [
    "${data.aws_availability_zones.zones.names[0]}",
    "${data.aws_availability_zones.zones.names[1]}",
  ]

  private_subnets  = var.private_subnets
  public_subnets   = var.public_subnets
  database_subnets = var.database_subnets

  create_database_subnet_group = false
  enable_dns_support           = true
  enable_nat_gateway           = true
  single_nat_gateway           = true
  one_nat_gateway_per_az       = false
  tags                         = var.default_tags
}

