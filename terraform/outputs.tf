output "alb_external_fqdn" {
  value = aws_lb.front_end.dns_name
}

output "s3transfer_bucket_name" {
  value = aws_s3_bucket.s3transfer.id
}
output "s3transfer_bucket_arn" {
  value = aws_s3_bucket.s3transfer.arn
}
