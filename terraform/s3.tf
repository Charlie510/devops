resource "aws_s3_bucket" "s3transfer" {
  bucket = "v-nova-devops-test-s3transfer"
  acl    = "private"

  tags = var.default_tags
}
