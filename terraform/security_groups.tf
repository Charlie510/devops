# resource "aws_security_group" "packer_build" {
#   name        = "packer-build"
#   description = "Security Group for Packer Builds"
#   vpc_id      = "vpc-090377310c7acf853"

#   egress {
#     from_port   = 0
#     to_port     = 0
#     protocol    = "-1"
#     cidr_blocks = ["0.0.0.0/0"]
#   }

#   ingress {
#     from_port   = 22
#     to_port     = 22
#     protocol    = "tcp"
#     cidr_blocks = ["0.0.0.0/0"]
#   }

#   tags = {
#     Name = "packer-build"
#   }
# }
resource "aws_security_group" "bastion" {
  name        = "packer-build"
  description = "Security Group for Packer Builds"
  vpc_id      = module.vpc.vpc_id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "bastion-sg"
  }
}
resource "aws_security_group" "web_server" {
  name        = "web-server"
  description = "Security Group for Web Server"
  vpc_id      = module.vpc.vpc_id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 80
    to_port   = 80
    protocol  = "tcp"
    security_groups = [
      aws_security_group.alb.id
    ]
    description = "http in"
  }
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    description = "ssh in from bastion"
    security_groups = [
      aws_security_group.bastion.id
    ]
  }

  tags = {
    Name = "web-server-sg"
  }
}
resource "aws_security_group" "alb" {
  name        = "alb"
  description = "Security Group for Application Load Balancer"
  vpc_id      = module.vpc.vpc_id

  # can't use due to cyclic dependency:
  # egress {
  #   from_port   = 80
  #   to_port     = 80
  #   protocol    = "TCP"
  #   security_groups = [
  #     aws_security_group.web_server.id
  #   ]
  # }
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    description = "http in"
  }
  tags = {
    Name = "alb-sg"
  }
}
# required to stop cyclic dependency:
resource "aws_security_group_rule" "alb-to-web-server" {
  type                     = "egress"
  from_port                = 80
  to_port                  = 80
  protocol                 = "tcp"
  security_group_id        = aws_security_group.alb.id
  source_security_group_id = aws_security_group.web_server.id
}
